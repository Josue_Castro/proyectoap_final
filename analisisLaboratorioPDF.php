<?php
require_once "fpdf/fpdf.php";
require_once "logica/laboratorio.php";
require_once "logica/cliente.php";

$pdf = new FPDF("P", "mm", "Letter");
$pdf -> SetFont("times", "B", 20);
$pdf -> AddPage();
$pdf -> Image('img/pagina2.jpg',0,0,220);
$pdf ->SetXY(0, 0);
$pdf->SetFillColor(0, 191, 255);
$pdf ->Rect(0,0,225,20,'F');

$pdf -> Cell(210, 20, "Laboratorio Clinico",0, 2, "C");


$Cliente = new cliente($_GET["idCliente"]);
$Cliente -> consultarPDF();


$pdf -> SetFont("times", "B", 20);
$pdf->SetXY(25,45);
$pdf -> Cell(170, 20, "Cliente",0, 2, "C");
$pdf ->Ln();
$pdf -> SetFont("times", "B", 14);
$pdf ->Ln();
$pdf->SetXY(50,40);
$pdf ->Cell(80,75,"Nombre: ".$Cliente->getNombre(),0);
$pdf ->Cell(110,75,"Apellido: ".$Cliente->getApellido(),0);
$pdf ->Ln();
$pdf->SetXY(50,50);
$pdf ->Cell(80,75,"Id: ".$Cliente->getIdentificacion(),0);
$pdf ->Cell(110,75,"Sexo: ".$Cliente->getSexo(),0);
$pdf ->Ln();
$pdf->SetXY(50,60);
$pdf ->Cell(80,75,"RH: ".$Cliente->getRh(),0);
$pdf ->Cell(110,75,"Correo: ".$Cliente->getCorreo(),0);
$pdf ->Ln();

$laboratorio = new laboratorio();
$laboratorios = $laboratorio -> consultarLaboratoriopdf($_GET["idCliente"]);

$pdf -> SetFont("times", "B", 20);
$pdf->SetY(110);
$pdf -> Cell(180, 20, "Resultados",0, 2, "C");

$pdf -> SetFont("times", "B", 14);
$pdf->SetXY(40,130);
$pdf ->Cell(50,10,"Examen",0);
$pdf ->Cell(50,10,"Resultado",0);
$pdf ->Cell(50,10,"Valor de referencia",0);

$i=1;
foreach ($laboratorios as $laboratorioActual){
$pdf -> SetFont("times", "B", 14);
$pdf ->Ln();
$pdf->SetXY(50,60);
$pdf ->Cell(180,20,"Fecha: ".$laboratorioActual->getFecha(),0);
$pdf ->Ln();


    $Colesterol = $laboratorioActual -> getColesterol();
    $Trigliceridos = $laboratorioActual -> getTrigliceridos();
    $Hemoglobina = $laboratorioActual -> getHemoglobina();
    $Linfositos = $laboratorioActual -> getLinfositos();
    $Glucosa = $laboratorioActual -> getGlucosa();
    $Observaciones = $laboratorioActual -> getObservaciones();
    $idCliente = $laboratorioActual -> getIdCliente();
    $idAnalista = $laboratorioActual -> getIdAnalista();

    $pdf->SetXY(40,140);
    $pdf ->Cell(90,10,"Colesterol............................".$Colesterol."..........................................50 - 220",0);

    $pdf ->Ln();
    $pdf->SetXY(40,150);
    $pdf ->Cell(90,10,"Trigliceridos.......................".$Trigliceridos."..........................................40 - 200",0);
    $pdf ->Ln();
    $pdf->SetXY(40,160);
    $pdf ->Cell(90,10,"Hemoglobina......................".$Hemoglobina."..........................................12 - 16",0);
    $pdf ->Ln();
    $pdf->SetXY(40,170);
    $pdf ->Cell(90,10,"Linfositos............................".$Linfositos."..........................................1.5 - 6.5",0);
    $pdf ->Ln();
    $pdf->SetXY(40,180);
    $pdf ->Cell(90,10,"Glucosa...............................".$Glucosa."..........................................80 - 115",0);
    $pdf ->Ln();
    $pdf->SetXY(40,190);
    $pdf ->Cell(90,10,"Observaciones: ".$Observaciones,0);

    $pdf ->Ln();

}

//idLaboratorio,Fecha,Colesterol,Trigliceridos,Hemoglobina,Linfositos,Glucosa,Observaciones,Cliente_idCliente,Analista_Clinico_idAnalista
$pdf -> Output();


?>
