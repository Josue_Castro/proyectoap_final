<?php
$idAnalista = $_GET["idAnalista"];
$nuevoEstado = $_GET["nuevoEstado"];

echo "<a id='cambiarEstado" . $idAnalista . "' href='#' >" . (($nuevoEstado==1)?"<span class='fas fa-user-times' data-toggle='tooltip' data-placement='left' title='Deshabilitar'></span>":"<span class='fas fa-user-check' data-toggle='tooltip' data-placement='left' title='Habilitar'></span>") . "</a>";
?>

<script>
$(document).ready(function(){
	$("#cambiarEstado<?php echo $idAnalista ?>").click(function(e){
		$('[data-toggle="tooltip"]').tooltip('hide');
		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/analista/ajax/cambiarEstadoAnalistaAjax.php") ?>&idAnalista=<?php echo $idAnalista ?>&nuevoEstado=<?php echo (($nuevoEstado==1)?"0":"1")?>";		
		$("#icono<?php echo $idAnalista ?>").load(url);
		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/analista/ajax/cambiarEstadoAnalistaAccionAjax.php") ?>&idAnalista=<?php echo $idAnalista ?>&nuevoEstado=<?php echo (($nuevoEstado==1)?"0":"1")?>";
		$("#accion<?php echo $idAnalista ?>").load(url);
	});		
});
</script>

