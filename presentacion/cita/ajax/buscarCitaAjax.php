<?php

$filtro = $_GET["filtro"];
$Cita  = new cita("","",$_SESSION["id"]);
$Citas = $Cita  -> consultarFiltro($filtro);
?>

<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white text-center" style="background-color: #437DB2;">
					<h4>Consultar Cita</h4>
				</div>
				<div class="text-right"><?php echo count($Citas) ?> registros encontrados</div>
              	<div class="card-body">
              	 <div class="table-responsive">
					<table id="example" class="table table-striped table-bordered text-center" cellspacing="0" width="100%">
						<tr>
							<th>#</th>
							<th>Fecha</th>
							<th>Hora</th>
							<th></th>
						</tr>
						<?php 
						$i=1;
						foreach($Citas as $CitaActual){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $CitaActual -> getFecha() . "</td>";
						    echo "<td>" . $CitaActual -> getHora() . "</td>";
						    ?>
						    <td><a class="dropdown-item" href="reporteCitaPDF.php?idCita=<?php echo$CitaActual ->getIdCita()?>" target="_blank"><i class="fas fa-file-pdf"></i></a></td>
						    <?php 
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				  </div>	
				</div>
            </div>
		</div>
	</div>
</div>