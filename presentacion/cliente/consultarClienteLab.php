<?php
$cliente = new Cliente();
$clientes = $cliente -> consultarTodos();
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Clientes</h4>
				</div>

        <div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>Idenficicacion</th>
							<th>PDF</th>
						</tr>
						<?php
						$i=1;
						foreach($clientes as $clienteActual){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $clienteActual -> getNombre() . "</td>";
						    echo "<td>" . $clienteActual -> getApellido() . "</td>";
								echo "<td>" . $clienteActual -> getIdentificacion() . "</td>";

								?>
								<td><a href="analisisLaboratorioPDF.php?idCliente=<?php echo$clienteActual ->getIdCliente()?>" target="_blank" data-toggle='tooltip' data-placement='left' title='PDF'><i class="fas fa-file-pdf"></i></a></td>
								<?php
								echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>
