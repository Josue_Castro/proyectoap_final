<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
			<div class="card">
				<div class="card-header text-white bg-dark text-center">
					<h4>Buscar Cliente/Historia</h4>
				</div>
				<div class="card-body">
					<input type="text" id="filtro" class="form-control"
						placeholder="Palabra clave">
				</div>
			</div>
		</div>
	</div>
</div>
<div id="resultados"></div>
<script>
$(document).ready(function(){
    $("#filtro").keyup(function() {
	    	var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/cliente/ajax/buscarHistoriaClienteAjax.php") ?>&filtro=" + $(this).val();
    		$("#resultados").load(url);
    });
});
</script>
