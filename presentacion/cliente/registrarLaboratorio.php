<?php

$colesterol = "";
if (isset($_POST["colesterol"])) {
    $colesterol = $_POST["colesterol"];
}
$ob_colesterol = "";
if (isset($_POST["ob_colesterol"])) {
    $ob_colesterol = $_POST["ob_colesterol"];
}
$trigliceridos = "";
if (isset($_POST["trigliceridos"])) {
    $trigliceridos = $_POST["trigliceridos"];
}
$ob_trigliceridos = "";
if (isset($_POST["ob_trigliceridos"])) {
    $ob_trigliceridos = $_POST["ob_trigliceridos"];
}
$hemoglobina = "";
if (isset($_POST["hemoglobina"])) {
    $hemoglobina = $_POST["hemoglobina"];
}
$ob_hemoglobina = "";
if (isset($_POST["ob_hemoglobina"])) {
    $ob_hemoglobina = $_POST["ob_hemoglobina"];
}
$linfositos = "";
if (isset($_POST["linfositos"])) {
    $linfositos = $_POST["linfositos"];
}
$ob_linfositos = "";
if (isset($_POST["ob_linfositos"])) {
    $ob_linfositos = $_POST["ob_linfositos"];
}
$glucosa = "";
if (isset($_POST["glucosa"])) {
    $glucosa = $_POST["glucosa"];
}
$ob_glucosa = "";
if (isset($_POST["ob_glucosa"])) {
    $ob_glucosa = $_POST["ob_glucosa"];
}
$descripcion = "";
if (isset($_POST["descripcion"])) {
    $descripcion = $_POST["descripcion"];
}




if (isset($_POST["crear"])) {

    $IdCita=$_GET["idCita"];
    $Cita = new cita($IdCita);
    $Cita->consultar();
    date_default_timezone_set("America/Bogota");
    $fecha=date("Y-m-d (H:i:s)");
    $fechaRealizado=date("Y-m-d");
    $horaRealizado=date("H:i:s");
    $log = new log("","Analista","Registro Laboratorio","Colesterol:".$colesterol."\n Trigliceridos:".$trigliceridos."\n Hemoglobina:".$hemoglobina."\nLinfositos:".$linfositos."\nGlucosa:".$glucosa,$fechaRealizado,$horaRealizado,$_SESSION["id"]);
    $log ->insertarAnalista();



    $laboratorio = new laboratorio("",$fecha,$colesterol,$ob_colesterol,$trigliceridos,$ob_trigliceridos,$hemoglobina,$ob_hemoglobina,$linfositos,$ob_linfositos,$glucosa,$ob_glucosa,$descripcion, $Cita->getIdCliente(), $_SESSION["id"],$IdCita);
    $laboratorio->registrar();

}

?>

<div class="container mt-4">
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Laboratorio</h4>
				</div>
				<div class="card-body">
					<form action="index.php?pid=<?php echo base64_encode("presentacion/cliente/registrarLaboratorio.php"). "&idCita=" . $_GET["idCita"] ?>" method="post">
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="inputEmail4">Colesterol</label> <input
										type="number" name="colesterol" class="form-control">
								</div>
								<div class="form-group col-md-6">
									<label for="inputPassword4">Trigliceridos</label> <input
										type="number" name="trigliceridos" class="form-control">
								</div>
							</div>
              <div class="form-row">
								<div class="form-group col-md-6">
									<label for="inputEmail4">Observaciones Colesterol</label> <input
										type="textarea" name="ob_colesterol" class="form-control">
								</div>
								<div class="form-group col-md-6">
									<label for="inputPassword4">Observaciones Trigliceridos</label> <input
										type="textarea" name="ob_trigliceridos" class="form-control">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="inputEmail4">Hemoglobina</label> <input
										type="number" name="hemoglobina" class="form-control">
								</div>
								<div class="form-group col-md-6">
									<label for="inputPassword4">Linfositos</label> <input
										type="number" name="linfositos" class="form-control">
								</div>
							</div>
              <div class="form-row">
								<div class="form-group col-md-6">
									<label for="inputEmail4">Observaciones Hemoglobina</label> <input
										type="textarea" name="ob_hemoglobina" class="form-control">
								</div>
								<div class="form-group col-md-6">
									<label for="inputPassword4">Observaciones Linfositos</label> <input
										type="textarea" name="ob_linfositos" class="form-control">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="inputEmail4">Glucosa</label> <input
									type="number" name="glucosa" class="form-control">
								</div>
  							</div>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="inputEmail4">Observaciones Glucosa</label> <input
                      type="textarea" name="ob_glucosa" class="form-control">
                  </div>
							</div>
							<div class="form-group">
								<label for="exampleFormControlTextarea1">Descripcion</label>
								<textarea class="form-control" name="descripcion"
									rows="3"></textarea>
							</div>
							<button type="submit" name="crear" class="btn btn-dark">Registrar <i class="fas fa-plus-circle"></i></button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
