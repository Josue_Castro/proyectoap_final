<?php
$Motivo = "";
if (isset($_POST["Motivo"])) {
    $Motivo = $_POST["Motivo"];
}

$Antecedentes = "";
if (isset($_POST["Antecedentes"])) {
    $Antecedentes = $_POST["Antecedentes"];
}

$Observaciones = "";
if (isset($_POST["Observaciones"])) {
    $Observaciones = $_POST["Observaciones"];
}


if (isset($_POST["crear"])) {
    date_default_timezone_set("America/Bogota");
    $fecha=date("Y-m-d");
    $hora=date("H:i:s");

    $historia = new historia("", $Motivo, $Antecedentes, $_GET["idCliente"],$fecha,$hora, $Observaciones);
    $historia->registrar();

    $fechaRealizado=date("Y-m-d");
    $horaRealizado=date("H:i:s");
    $log = new log("","Medico","Registrar en Historia","Motivo:".$Motivo."\n Antecedentes:".$Antecedentes."\n Observaciones:".$Observaciones."\nFecha:".$fecha."\nHora:".$hora,$fechaRealizado,$horaRealizado,$_SESSION["id"]);
    $log -> insertarMedico();

}

$Cliente=new cliente($_GET["idCliente"]);
$Cliente->consultar();

?>

<div class="container mt-4">
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Registrar Historia</h4>
				</div>
				<div class="card-body">
					<form
						action="index.php?pid=<?php echo base64_encode("presentacion/cliente/editarHistoria.php"). "&idCliente=" . $_GET["idCliente"] ?>"
						method="post">

						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="inputEmail4">Nombre</label> <input type="text"
									class="form-control" value="<?php echo $Cliente -> getNombre() ?>">
							</div>
							<div class="form-group col-md-6">
								<label for="inputPassword4">Apellido</label> <input
									type="text" class="form-control" value="<?php echo $Cliente -> getApellido() ?>">
							</div>
						</div>

						<div class="form-group">
							<label for="exampleFormControlTextarea1">Motivo</label>
							<textarea class="form-control" name="Motivo" rows="3"></textarea>
						</div>
						<div class="form-group">
							<label for="exampleFormControlTextarea1">Antecedentes</label>
							<textarea class="form-control" name="Antecedentes" rows="3"></textarea>
						</div>
						<div class="form-group">
							<label for="exampleFormControlTextarea1">Observaciones</label>
							<textarea class="form-control" name="Observaciones" rows="3"></textarea>
						</div>
						<button type="submit" name="crear" class="btn btn-dark">
							Registrar <i class="fas fa-plus-circle"></i>
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
