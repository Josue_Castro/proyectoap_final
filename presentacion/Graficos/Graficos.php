<?php
$medicoGrafica = new medicoGrafica();
$MedicosGraficos = $medicoGrafica->GraficoEspecialidades();

$FechaGrafica = new FechasGrafica();
$FechasGraficos = $FechaGrafica->GraficoFechas();

$TipoSangre = new SangreGrafica();
$TiposSangre = $TipoSangre->GraficoSangre();

$Sexo = new GraficaSexo();
$Sexos = $Sexo->GraficaBarrasSexo();
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
			<div class="card ">
				<div class="card-header text-black bg-light text-center">
					<h4></h4>
				</div>
				<div class="card-body">
					<div class="row row-cols-1 row-cols-md-1">




					</div>

					<div class="row row-cols-1 row-cols-md-1">
						<div class="col mb-4">
							<div class="card">
								<html>
<head>
<script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable([
          ['Fecha', 'Cantidad de Citas'],
          <?php foreach($FechasGraficos as $FechaActual){ ?>


          ['<?php echo $FechaActual->getFecha() ?>',   <?php echo $FechaActual->getCantidad() ?>  ],

           <?php } ?>
        ]);

        var options = {
          title: 'Chess opening moves',
          width: 900,
          legend: { position: 'none' },
          chart: { title: 'Chess opening moves',
                   subtitle: 'popularity by percentage' },
          bars: 'horizontal', // Required for Material Bar Charts.
          axes: {
            x: {
              0: { side: 'top', label: 'Percentage'} // Top x-axis.
            }
          },
          bar: { groupWidth: "90%" }
        };

        var chart = new google.charts.Bar(document.getElementById('top_x_div'));
        chart.draw(data, options);
      };
    </script>
</head>
<body>
	<div id="top_x_div" style="width: 800px; height: 300px;"></div>
</body>
								</html>

								<div class="card-body">
									<h5 class="card-title text-center">Fechas</h5>
									<p class="card-text text-center">Esta grfica permite observar
										la cantidad de citas que se encuentran progrmadas para
										determninadas fechas.</p>
								</div>
							</div>
						</div>



					</div>





					<div class="row row-cols-1 row-cols-md-2">
						<div class="col mb-8">

							<div
								class="card align-items-center d-flex justify-content-center">
								<html>
<head>
<script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable([
          ['Move', 'Percentage'],
          <?php foreach($TiposSangre as $TipoActual){ ?>


          ['<?php echo $TipoActual->getSangre() ?>',   <?php echo $TipoActual->getCantidad() ?>  ],

           <?php } ?>
        ]);


        var options = {
          width: 200,
          legend: { position: 'none' },
          chart: {
            title: 'Chess opening moves',
            subtitle: 'popularity by percentage' },
          axes: {
            x: {
              0: { side: 'top', label: 'Tipo de Sangre'} // Top x-axis.
            }
          },
          bar: { groupWidth: "90%" }
        };

        var chart = new google.charts.Bar(document.getElementById('top_x_div_sangre'));
        // Convert the Classic options to Material options.
        chart.draw(data, google.charts.Bar.convertOptions(options));
      };
    </script>
</head>
<body>
	<div id="top_x_div_sangre" style="width: 500px; height: 400px;"></div>
</body>
								</html>



								<div class="card-body">
									<h5 class="card-title text-center">Tipo de Sangre</h5>
									<p class="card-text text-center">Esta grafica permite observar
										la cantidad de los tipos de sangre con respecto a los
										pacientes registrrados en el sistema.</p>
								</div>
							</div>
						</div>




						<div class="col mb-8">

							<div
								class="card align-items-center d-flex justify-content-center">
								<html>
<head>

<script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable([
          ['Move', 'Percentage'],
          <?php foreach($Sexos as $SexosActual){ ?>


          ['<?php echo $SexosActual->getSexo() ?>',   <?php echo $SexosActual->getCantidad() ?>  ],

           <?php } ?>
        ]);


        var options = {
          width: 200,
          legend: { position: 'none' },
          chart: {
            title: 'Chess opening moves',
            subtitle: 'popularity by Porcentaje' },
          axes: {
            x: {
              0: { side: 'top', label: 'Sexo'} // Top x-axis.
            }
          },
          bar: { groupWidth: "90%" }
        };

        var chart = new google.charts.Bar(document.getElementById('top_x_div_sexo'));
        // Convert the Classic options to Material options.
        chart.draw(data, google.charts.Bar.convertOptions(options));
      };
    </script>
</head>
<body>
	<div id="top_x_div_sexo" style="width: 500px; height: 400px;"></div>
</body>
								</html>



								<div class="card-body">
									<h5 class="card-title text-center">Sexo</h5>
									<p class="card-text text-center">Esta grafica permite observar
										la cantidad de usuario organizados de acuerdo a su sexo.</p>
								</div>
							</div>
						</div>


					</div>




				</div>
			</div>
		</div>
	</div>
</div>
