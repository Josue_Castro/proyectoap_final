<?php
$laboratorio = new laboratorio("", "", "", "", "", "", "", "", "", "", "", "", "", $_SESSION["id"]);
$laboratorios = $laboratorio->consultarGrafico();

?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
			<div class="card ">
				<div class="card-header text-black bg-light text-center">
					<h4></h4>
				</div>
				<div class="card-body">
					<div class="row row-cols-1 row-cols-md-1"></div>

					<div class="row row-cols-1 row-cols-md-1">
						<div class="col mb-4">
							<div class="card">
							<html>
  <head>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawVisualization);

      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([

        ['Examenes', 'Colesterol', 'Triglicerios','Hemoglobina','Linfositos','Glucosa'],
          <?php foreach($laboratorios as $laboratoriosActual){ ?>


          ['<?php echo $laboratoriosActual->getFecha() ?>',<?php echo $laboratoriosActual->getColesterol() ?>,<?php echo $laboratoriosActual->getTrigliceridos() ?>,<?php echo $laboratoriosActual->getHemoglobina() ?>,<?php echo $laboratoriosActual->getLinfositos() ?>,<?php echo $laboratoriosActual->getGlucosa() ?>  ],

           <?php } ?>
        ]);

        var options = {
          title : 'Resultados de Examenes',
          vAxis: {title: 'Porcentaje(mg/dl)'},
          hAxis: {title: 'Examen'},
          seriesType: 'bars',
          series: {5: {type: 'line'}}        };

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="chart_div" style="width: 800px; height: 500px;"></div>
  </body>
</html>


								<div class="card-body">
									<h5 class="card-title text-center">Examenes</h5>
									<p class="card-text text-center">Esta grfica permite observar
										y comparar los resultados de sus exemnes de laboratorio.</p>
								</div>
							</div>
						</div>



					</div>

				</div>
			</div>
		</div>
	</div>
