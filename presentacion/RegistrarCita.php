<?php
$hora = "";
if (isset($_POST["Hora"])) {
    $hora = $_POST["Hora"];
}
$fecha = "";
if (isset($_POST["Fecha"])) {
    $fecha = $_POST["Fecha"];
}

if(isset($_POST["registrar"])){
    //"Location:reporteCitaPDF.php");
    date_default_timezone_set("America/Bogota");
    $fechaRealizado=date("Y-m-d");
    $horaRealizado=date("H:i:s");

    $Cita = new cita("",$_POST["Medico"],$_SESSION["id"],$fecha,$hora,$fechaRealizado,$horaRealizado);
    $Cita->registrar();

    $log = new log("","Cliente","Registro Cita","Fecha:".$fecha."\n Hora:".$hora."\n Medico:".$_POST["Medico"],$fechaRealizado,$horaRealizado,$_SESSION["id"]);
    $log ->insertarCliente();

}


$medico = new medico();
$medicos = $medico->consultarTodos();
?>
<div class="container mt-4">
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Citas</h4>
				</div>
				<div class="card-body">
					<form action="index.php?pid=<?php echo base64_encode("presentacion/RegistrarCita.php") ?>" method="post" enctype="multipart/form-data">
						<div class="form-row">
							<div class="form-group col-md-8">
							<label for="inputState">Medico</label>
							<select class="form-control" name="Medico">
								<?php
								foreach($medicos as $medicoActual){


								    echo "<option value=".$medicoActual-> getIdMedico().">".$medicoActual->getNombre()."</option>";
								 }?>

							</select>
						</div>

						</div>
						<div class="form-row">
							<div class="form-group col-md-4">
								<label for="inputZip">Fecha</label> <input type="date" name="Fecha"
									class="form-control" id="inputZip" required>
							</div>
							<div class="form-group col-md-4">
								<label for="inputZip">Hora</label> <input type="time" name="Hora"
									class="form-control" id="inputZip" required>
							</div>
						</div>
						<div class="form-group">
						</div>

						<button type="submit" name="registrar" class="btn btn-dark">Programar Cita</button>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>
