<?php
$administrador= new Administrador();
$administradores = $administrador -> consultarTodos();
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Consultar Administradores</h4>
				</div>
				<div class="text-right"><?php echo count($administradores) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped table-responsive-md">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>Correo</th>
							<th>Estado</th>
							<th>Servicios</th>
						</tr>
						<?php
						$i=1;
						foreach($administradores as $administradorActual){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $administradorActual -> getNombre() . "</td>";
						    echo "<td>" . $administradorActual -> getApellido() . "</td>";
						    echo "<td>" . $administradorActual -> getCorreo() . "</td>";
						    echo "<td>" . (($administradorActual -> getEstado()==1)?"<div id='icono" . $administradorActual -> getIdAdministrador() . "'><span class='fas fa-check-circle' data-toggle='tooltip' data-placement='left' title='Habilitado'></span></div>":(($administradorActual -> getEstado()==0)?"<div id='icono" . $administradorActual -> getIdAdministrador() . "'><span class='fas fa-times-circle' data-toggle='tooltip' data-placement='left' title='Deshabilitado'></span></div>":"<span class='fas fa-ban' data-toggle='tooltip' data-placement='left' title='Inactivo'></span>")) . "</td>";
						    echo "<td><div id='accion" . $administradorActual -> getIdAdministrador() . "'><a id='cambiarEstado" . $administradorActual -> getIdAdministrador() . "' href='#' >" . (($administradorActual -> getEstado()==1)?"<span class='fas fa-user-times' data-toggle='tooltip' data-placement='left' title='Deshabilitar'></span>":(($administradorActual -> getEstado()==0)?"<span class='fas fa-user-check' data-toggle='tooltip' data-placement='left' title='Habilitar'></span>":"")) . "</a>";
						?>
                        <script>
                        $(document).ready(function(){
                        	$("#cambiarEstado<?php echo $administradorActual -> getIdAdministrador() ?>").click(function(e){
                        		$('[data-toggle="tooltip"]').tooltip('hide');
                        		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/ajax/cambiarEstadoAdministradorAjax.php") ?>&idAdministrador=<?php echo $administradorActual -> getIdAdministrador() ?>&nuevoEstado=<?php echo (($administradorActual -> getEstado()==1)?"0":"1")?>";
                        		$("#icono<?php echo $administradorActual -> getIdAdministrador() ?>").load(url);
                        		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/ajax/cambiarEstadoAccionAjax.php") ?>&idAdministrador=<?php echo $administradorActual -> getIdAdministrador() ?>&nuevoEstado=<?php echo (($administradorActual -> getEstado()==1)?"0":"1")?>";
                        		$("#accion<?php echo $administradorActual -> getIdAdministrador() ?>").load(url);
                        	});
                        });
                        </script>
						<?php
						    echo "</div></td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>
