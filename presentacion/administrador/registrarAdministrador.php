<?php
$nombre = "";
if (isset($_POST["nombre"])) {
    $nombre = $_POST["nombre"];
}
$apellido = "";
if (isset($_POST["apellido"])) {
    $apellido = $_POST["apellido"];
}
$correo = "";
if (isset($_POST["correo"])) {
    $correo = $_POST["correo"];
}
$clave = "";
if (isset($_POST["clave"])) {
    $clave = $_POST["clave"];
}

if (isset($_POST["crear"])) {

  if ($_FILES["imagen"]["name"] != "") {
        $rutaLocal = $_FILES["imagen"]["tmp_name"];
        $tipo = $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "img/" . $tiempo->getTimestamp() . (($tipo == "image/png") ? ".png" : ".jpg");
        copy($rutaLocal, $rutaRemota);
        $administrador = new administrador("", $nombre, $apellido, "", $correo, $clave,$rutaRemota);
        $administrador->registrar();

    }else {
      $administrador = new administrador("", $nombre, $apellido, "", $correo, $clave,"");
      $administrador->registrar();
    }

    date_default_timezone_set("America/Bogota");
    $fechaRealizado=date("Y-m-d");
    $horaRealizado=date("H:i:s");
    $log = new log("","Administrador","Registro Administrador","Nombre:".$nombre."\n Apellido:".$apellido."\n Correo:".$correo, $fechaRealizado,$horaRealizado,$_SESSION["id"]);
    $log ->insertarAdmin();
}
?>

<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Registrar Administrador</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/administrador/registrarAdministrador.php") ?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nombre</label>
							<input type="text" name="nombre" class="form-control" value="<?php echo $nombre ?>" required>
						</div>
						<div class="form-group">
							<label>Apellido</label>
							<input type="text" name="apellido" class="form-control" value="<?php echo $apellido ?>" required>
						</div>
						<div class="form-group">
							<label>Correo</label>
							<input type="mail" name="correo" class="form-control" value="<?php echo $correo ?>" required>
						</div>
						<div class="form-group">
							<label>Clave</label>
							<input type="password" name="clave" class="form-control" value="<?php echo $clave ?>" required>
						</div>
            <div class="form-group">
							<label>Foto</label>
							<input type="file" name="imagen" class="form-control" value="<?php echo $foto ?>">
						</div>


						<button type="submit" name="crear" class="btn btn-dark">Registrar <i class="fas fa-plus-circle"></i></button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>
