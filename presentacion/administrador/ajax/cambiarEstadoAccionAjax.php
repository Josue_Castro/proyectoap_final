<?php
$idAdministrador = $_GET["idAdministrador"];
$nuevoEstado = $_GET["nuevoEstado"];

echo "<a id='cambiarEstado" . $idAdministrador . "' href='#' >" . (($nuevoEstado==1)?"<span class='fas fa-user-times' data-toggle='tooltip' data-placement='left' title='Deshabilitar'></span>":"<span class='fas fa-user-check' data-toggle='tooltip' data-placement='left' title='Habilitar'></span>") . "</a>";
?>

<script>
$(document).ready(function(){
	$("#cambiarEstado<?php echo $idAdministrador ?>").click(function(e){
		$('[data-toggle="tooltip"]').tooltip('hide');
		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/ajax/cambiarEstadoAdministradorAjax.php") ?>&idAdministrador=<?php echo $idAdministrador ?>&nuevoEstado=<?php echo (($nuevoEstado==1)?"0":"1")?>";
		$("#icono<?php echo $idAdministrador ?>").load(url);
		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/ajax/cambiarEstadoAccionAjax.php") ?>&idAdministrador=<?php echo $idAdministrador ?>&nuevoEstado=<?php echo (($nuevoEstado==1)?"0":"1")?>";
		$("#accion<?php echo $idAdministrador ?>").load(url);
	});
});
</script>
