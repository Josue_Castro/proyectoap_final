<?php
$filtro = $_GET["filtro"];
$log  = new log();
$Logs = $log  -> consultarFiltroMedico($filtro);
?>

<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-dark text-center">
					<h4>Consultar Medico</h4>
				</div>
				<div class="text-right"><?php echo count($Logs) ?> registros encontrados</div>
              	<div class="card-body">
              	 <div class="table-responsive">
					<table id="example" class="table table-striped table-bordered text-center" cellspacing="0" width="100%">
						<tr>
							<th>#</th>
							<th>Accion</th>
							<th>Datos</th>
							<th>Fecha</th>
							<th>Hora</th>
							<th>Medico</th>
							<th></th>
						</tr>
						<?php
						$i=1;
						foreach($Logs as $LogActual){
						    $Medico=new medico($LogActual -> getIdactor());
						    $Medico->consultar();
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $LogActual -> getAccion() . "</td>";
						    echo "<td>" . $LogActual -> getDatos() . "</td>";
						    echo "<td>" . $LogActual -> getFecha() . "</td>";
						    echo "<td>" . $LogActual -> getHora() . "</td>";
						    echo "<td>" . $Medico->getNombre()." ".$Medico->getApellido() . "</td>";
						    echo "<td><button id='cambiarEstado" . $LogActual -> getIdLog() . "' type='button' class='btn btn-dark editbtn' data-toggle='modal' data-target='#imodal".$LogActual -> getIdLog()."' ><i class='fas fa-info-circle'></i></button></td>";
						    ?>
                        <script>
                        $(document).ready(function(){
                        	$("#cambiarEstado<?php echo $LogActual -> getIdLog() ?>").click(function(e){
                        		$('[data-toggle="tooltip"]').tooltip('hide');
                        		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/medico/ajax/InfoAjax.php") ?>&idLog=<?php echo $LogActual -> getIdLog() ?>";
                        		$("#resultados<?php echo $LogActual -> getIdLog() ?>").load(url);
                        	});
                        });
                        </script>
						<?php
						    $i++;
						}
						?>
					</table>
				  </div>
				</div>
            </div>
		</div>
	</div>
</div>
<?php foreach($Logs as $LogActual){ ?>

<div class="modal fade" id="imodal<?php echo $LogActual -> getIdLog()?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Log Medico</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div id="resultados<?php echo $LogActual -> getIdLog()?>"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php
$i++;
}
?>
