<?php
$idMedico = $_GET["idMedico"];
$nuevoEstado = $_GET["nuevoEstado"];

echo "<a id='cambiarEstado" . $idMedico . "' href='#' >" . (($nuevoEstado==1)?"<span class='fas fa-user-times' data-toggle='tooltip' data-placement='left' title='Deshabilitar'></span>":"<span class='fas fa-user-check' data-toggle='tooltip' data-placement='left' title='Habilitar'></span>") . "</a>";
?>

<script>
$(document).ready(function(){
	$("#cambiarEstado<?php echo $idMedico ?>").click(function(e){
		$('[data-toggle="tooltip"]').tooltip('hide');
		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/medico/ajax/cambiarEstadoMedicoAjax.php") ?>&idMedico=<?php echo $idMedico ?>&nuevoEstado=<?php echo (($nuevoEstado==1)?"0":"1")?>";		
		$("#icono<?php echo $idMedico ?>").load(url);
		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/medico/ajax/cambiarEstadoMedicoAccionAjax.php") ?>&idMedico=<?php echo $idMedico ?>&nuevoEstado=<?php echo (($nuevoEstado==1)?"0":"1")?>";
		$("#accion<?php echo $idMedico ?>").load(url);
	});		
});
</script>


