<?php
$Nombre = "";
if (isset($_POST["Nombre"])) {
    $Nombre = $_POST["Nombre"];
}

$Apellido = "";
if (isset($_POST["Apellido"])) {
    $Apellido = $_POST["Apellido"];
}

$Especialidad = "";
if (isset($_POST["Especialidad"])) {
    $Especialidad = $_POST["Especialidad"];
}

$Correo = "";
if (isset($_POST["Correo"])) {
    $Correo = $_POST["Correo"];
}

$Clave = "";
if (isset($_POST["Clave"])) {
    $Clave = $_POST["Clave"];
}

$Tarjeta = "";
if (isset($_POST["Tarjeta"])) {
    $Tarjeta = $_POST["Tarjeta"];
}


if (isset($_POST["crear"])) {

    if ($_FILES["imagen"]["name"] != "") {
        $rutaLocal = $_FILES["imagen"]["tmp_name"];
        $tipo = $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "img/" . $tiempo->getTimestamp() . (($tipo == "image/png") ? ".png" : ".jpg");
        copy($rutaLocal, $rutaRemota);
        $Medico=new medico("",$Nombre,$Apellido,$Especialidad,"",$Correo,$Clave,$Tarjeta,$rutaRemota);
        $Medico->registrar();

    }else {
        $Medico=new medico("",$Nombre,$Apellido,$Especialidad,"",$Correo,$Clave,$Tarjeta);
        $Medico->registrar();
    }
    date_default_timezone_set("America/Bogota");
    $fechaRealizado=date("Y-m-d");
    $horaRealizado=date("H:i:s");
    $log = new log("","Administrador","Registro Medico","Nombre:".$Nombre."\n Apellido:".$Apellido."\n Correo:".$Correo."\nTarjeta:".$Tarjeta,$fechaRealizado,$horaRealizado,$_SESSION["id"]);
    $log ->insertarAdmin();

}
?>

<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Registrar Medico</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/medico/RegistrarMedico.php") ?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nombre</label>
							<input type="text" name="Nombre" class="form-control" value="<?php  ?>" required>
						</div>
						<div class="form-group">
							<label>Apellido</label>
							<input type="text" name="Apellido" class="form-control" min="1" value="<?php  ?>" required>
						</div>
						<div class="form-group">
							<label>Especialidad</label>
							<input type="text" name="Especialidad" class="form-control" min="1" value="<?php  ?>" required>
						</div>
						<div class="form-group">
							<label>Correo</label>
							<input type="mail" name="Correo" class="form-control" min="1" value="" required>
						</div>
						<div class="form-group">
							<label>Clave</label>
							<input type="password" name="Clave" class="form-control" min="1" value="" required>
						</div>
						<div class="form-group">
							<label>Tarjeta Profesional</label>
							<input type="text" name="Tarjeta" class="form-control" min="1" value="" required>
						</div>
						<div class="form-group">
							<label>Foto</label>
							<input type="file" name="imagen" class="form-control-file" >
						</div>

						<button type="submit" name="crear" class="btn btn-dark">Crear <i class="fas fa-plus-circle"></i></button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>
