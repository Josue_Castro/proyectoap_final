<?php
$Medico = new medico();
$Medicos = $Medico -> consultarTodos();
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-dark text-center">
					<h4>Consultar Medicos</h4>
				</div>
				<div class="text-right"><?php echo count($Medicos) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Correo</th>
							<th>Estado</th>
							<th>Servicios</th>
						</tr>
						<?php
						$i=1;
						foreach($Medicos as $MedicoActual){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $MedicoActual -> getNombre()." ".$MedicoActual -> getApellido(). "</td>";
						    echo "<td>" . $MedicoActual -> getEspecialidad() . "</td>";
						    echo "<td>" . (($MedicoActual -> getEstado()==1)?"<div id='icono" . $MedicoActual -> getIdMedico() . "'><span class='fas fa-check-circle' data-toggle='tooltip' data-placement='left' title='Habilitado'></span></div>":(($MedicoActual -> getEstado()==0)?"<div id='icono" . $MedicoActual -> getIdMedico() . "'><span class='fas fa-times-circle' data-toggle='tooltip' data-placement='left' title='Deshabilitado'></span></div>":"<span class='fas fa-ban' data-toggle='tooltip' data-placement='left' title='Inactivo'></span>")) . "</td>";
						    echo "<td><div id='accion" . $MedicoActual -> getIdMedico() . "'><a id='cambiarEstado" . $MedicoActual -> getIdMedico() . "' href='#' >" . (($MedicoActual -> getEstado()==1)?"<span class='fas fa-user-times' data-toggle='tooltip' data-placement='left' title='Deshabilitar'></span>":(($MedicoActual -> getEstado()==0)?"<span class='fas fa-user-check' data-toggle='tooltip' data-placement='left' title='Habilitar'></span>":"")) . "</a>";
						    ?>
                        <script>
                        $(document).ready(function(){
                        	$("#cambiarEstado<?php echo $MedicoActual -> getIdMedico() ?>").click(function(e){
                        		$('[data-toggle="tooltip"]').tooltip('hide');
                        		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/medico/ajax/cambiarEstadoMedicoAjax.php") ?>&idMedico=<?php echo $MedicoActual -> getIdMedico() ?>&nuevoEstado=<?php echo (($MedicoActual -> getEstado()==1)?"0":"1")?>";
                        		$("#icono<?php echo $MedicoActual -> getIdMedico() ?>").load(url);
                        		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/medico/ajax/cambiarEstadoMedicoAccionAjax.php") ?>&idMedico=<?php echo $MedicoActual -> getIdMedico() ?>&nuevoEstado=<?php echo (($MedicoActual -> getEstado()==1)?"0":"1")?>";
                        		$("#accion<?php echo $MedicoActual -> getIdMedico() ?>").load(url);
                        	});
                        });
                        </script>
						<?php
						    echo "</div></td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>
