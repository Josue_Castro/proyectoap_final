<script>
$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
})
</script>

<?php
session_start();
require_once "logica/administrador.php";
require_once "logica/log.php";
require_once "logica/cliente.php";
require_once "logica/medico.php";
require_once "logica/cita.php";
require_once "logica/analista.php";

$pid = base64_decode($_GET["pid"]);
include $pid;
?>
