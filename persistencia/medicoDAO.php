<?php
class medicoDAO{
    private $idMedico;
    private $nombre;
    private $apellido;
    private $especialidad;
    private $estado;
    private $correo;
    private $clave;
    private $tarjetaProfesional;
    private $foto;

    public function medicoDAO($idMedico="",$nombre="",$apellido="",$especialidad="",$estado="",$correo="",$clave="",$tarjetaProfesional="",$foto=""){
        $this -> idMedico = $idMedico;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> especialidad = $especialidad;
        $this -> estado = $estado;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> tarjetaProfesional= $tarjetaProfesional;
        $this -> foto = $foto;

    }

    public function consultarTodos(){
        return "select idMedico, Nombre, Apellido, especialidad,estado
                from medico";
    }


    public function autenticar(){
        return "select idMedico, estado
                from medico
                where correo = '" . $this -> correo .  "' and clave = '" . md5($this -> clave) . "'";
    }

    public function consultar(){
        return "select Nombre, Apellido, especialidad, correo, tarjetaPro, foto
                from medico
                where idMedico = '" . $this -> idMedico .  "'";
    }

    public function registrar(){
        return "insert into medico (Nombre, Apellido, especialidad,correo,clave,tarjetaPro,foto)
                values ('" . $this -> nombre . "' , '" . $this -> apellido . "' , '" . $this -> especialidad . "' , '" . $this -> correo . "' , '" .  md5($this -> clave) . "', '" . $this -> tarjetaProfesional . "' , '" . $this -> foto . "')";
    }

    public function cambiarEstado(){
        return "update medico
                set estado = '" . $this -> estado . "'
                where idMedico = '" . $this -> idMedico .  "'";
    }


}

?>
