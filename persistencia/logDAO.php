<?php
class logDAO{

    private $idLog;
    private $actor;
    private $accion;
    private $datos;
    private $fecha;
    private $hora;
    private $Idactor;

    public function logDAO($idLog="",$actor="",$accion="",$datos="",$fecha="",$hora="",$Idactor=""){
        $this->idLog=$idLog;
        $this->actor=$actor;
        $this->accion=$accion;
        $this->datos=$datos;
        $this->fecha=$fecha;
        $this->hora=$hora;
        $this->Idactor=$Idactor;
    }

    public function insertarAdministrador(){
        return "insert into logadministrador(actor,accion,datos,fecha,hora,Administrador_idAdministrador)
                values ('"  . $this->actor. "', '" .$this->accion. "', '" . $this->datos . "', '" .$this->fecha . "', '".$this->hora."', '".$this->Idactor."')";
    }
    public function consultarFiltroAdministrador($filtro){
        return "select idLogAdministrador,actor,accion,datos,fecha,hora,Administrador_idAdministrador
                from logadministrador
                where accion like '%" . $filtro . "%' or datos like '" . $filtro . "%' or fecha like '" . $filtro . "%'";
    }

    public function consultarCliente(){
        return "select idLogCliente,actor,accion,datos,fecha,hora,Cliente_idCliente
                from logcliente";
    }
    public function consultarAdministrador(){
        return "select idLogAdministrador,actor,accion,datos,fecha,hora,Administrador_idAdministrador
                from logadministrador";
    }


    public function insertarCliente(){
        return "insert into logcliente(actor,accion,datos,fecha,hora,Cliente_idCliente)
                values ('"  . $this->actor. "', '" .$this->accion. "', '" . $this->datos . "', '" .$this->fecha . "', '".$this->hora."', '".$this->Idactor."')";
    }
    public function consultarFiltroCliente($filtro){
        return "select idLogCliente,actor,accion,datos,fecha,hora,Cliente_idCliente
                from logcliente
                where accion like '%" . $filtro . "%' or datos like '" . $filtro . "%' or fecha like '" . $filtro . "%'";
    }


    public function consultar(){
        return "select actor,accion,datos,fecha,hora,Cliente_idCliente
                from logcliente
                where idLogCliente = '" . $this -> idLog .  "'";
    }

    public function consultarLogAdmin(){
        return "select actor,accion,datos,fecha,hora,Administrador_idAdministrador
                from logadministrador
                where idLogAdministrador = '" . $this -> idLog .  "'";
    }


    public function consultarLogAnalista(){
        return "select actor,accion,datos,fecha,hora,Analista_Clinico_idAnalista
                from loganalista_clinico
                where idAnalista_Clinico = '" . $this -> idLog .  "'";
    }

    public function consultarLogMedico(){
        return "select actor,accion,datos,fecha,hora,Medico_idMedico
                from logmedico
                where idLogMedico = '" . $this -> idLog .  "'";
    }





    public function consultarAnalista(){
        return "select idAnalista_Clinico,actor,accion,datos,fecha,hora,Analista_Clinico_idAnalista
                from loganalista_clinico";
    }


    public function insertarAnalista(){
        return "insert into loganalista_clinico(actor,accion,datos,fecha,hora,Analista_Clinico_idAnalista)
                values ('"  . $this->actor. "', '" .$this->accion. "', '" . $this->datos . "', '" .$this->fecha . "', '".$this->hora."', '".$this->Idactor."')";
    }
    public function consultarFiltroAnalista($filtro){
        return "select idAnalista_Clinico,actor,accion,datos,fecha,hora,Analista_Clinico_idAnalista
                from loganalista_clinico
                where accion like '%" . $filtro . "%' or datos like '" . $filtro . "%' or fecha like '" . $filtro . "%'";
    }

    public function consultarMedico(){
        return "select idLogMedico,actor,accion,datos,fecha,hora,Medico_idMedico
                from logmedico";
    }


    public function insertarMedico(){
        return "insert into logmedico(actor,accion,datos,fecha,hora,Medico_idMedico)
                values ('"  . $this->actor. "', '" .$this->accion. "', '" . $this->datos . "', '" .$this->fecha . "', '".$this->hora."', '".$this->Idactor."')";
    }

    public function consultarFiltroMedico($filtro){
        return "select idLogMedico,actor,accion,datos,fecha,hora,Medico_idMedico
                from logmedico
                where accion like '%" . $filtro . "%' or datos like '" . $filtro . "%' or fecha like '" . $filtro . "%'";
    }
}
?>
