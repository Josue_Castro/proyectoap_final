<?php
require_once "persistencia/conexion.php";
require_once "persistencia/GraficaSexoDAO.php";
class GraficaSexo{
    private $Sexo;
    private $Cantidad;
    private $conexion;
    private $GraficaSexoDAO;

    public function getSexo()
    {
        return $this->Sexo;
    }

    public function getCantidad()
    {
        return $this->Cantidad;
    }

    public function GraficaSexo($Sexo="",$Cantidad=""){
        $this->Sexo=$Sexo;
        $this->Cantidad=$Cantidad;
        $this -> conexion = new conexion();
        $this -> GraficaSexoDAO = new GraficaSexoDAO($this->Sexo,$this->Cantidad);

    }

    public function GraficaBarrasSexo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> GraficaSexoDAO -> GraficaBarrasSexo());
        $Datos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new GraficaSexo($resultado[0],$resultado[1]);
            array_push($Datos, $p);
        }
        $this -> conexion -> cerrar();
        return $Datos;
    }

}

?>
