<?php
require_once "persistencia/conexion.php";
require_once "persistencia/medicoDAO.php";
class medico{
    private $idMedico;
    private $nombre;
    private $apellido;
    private $especialidad;
    private $estado;
    private $correo;
    private $clave;
    private $tarjetaProfesional;
    private $foto;
    private $conexion;
    private $medicoDAO;



    public function getIdMedico()
    {
        return $this -> idMedico;
    }

    public function getNombre()
    {
        return $this -> nombre;
    }

    public function getApellido()
    {
        return $this -> apellido;
    }

    public function getEspecialidad()
    {
        return $this -> especialidad;
    }

    public function getEstado()
    {
        return $this -> estado;
    }

    public function getCorreo()
    {
        return $this -> correo;
    }

    public function getClave()
    {
        return $this -> clave;
    }

    public function getTarjetaProfesional()
    {
        return $this -> tarjetaProfesional;
    }

    public function getFoto()
    {
        return $this -> foto;
    }


    public function medico($idMedico="",$nombre="",$apellido="",$especialidad="",$estado="",$correo="",$clave="",$tarjetaProfesional="",$foto=""){
        $this -> idMedico = $idMedico;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> especialidad = $especialidad;
        $this -> estado = $estado;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> tarjetaProfesional = $tarjetaProfesional;
        $this -> foto = $foto;
        $this -> conexion = new conexion();
        $this -> medicoDAO = new medicoDAO($this-> idMedico,$this-> nombre,$this-> apellido, $this-> especialidad, $this-> estado,$this-> correo,$this-> clave, $this-> tarjetaProfesional,$this-> foto);

    }

    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> medicoDAO -> consultarTodos());
        $medicos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new medico($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4]);
            array_push($medicos, $p);
        }
        $this -> conexion -> cerrar();
        return $medicos;
    }



    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> medicoDAO -> autenticar());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idMedico = $resultado[0];
            $this -> estado = $resultado[1];
            return true;
        }else {
            return false;
        }
    }
    

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> medicoDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> especialidad = $resultado[2];
        $this -> correo = $resultado[3];
        $this -> tarjetaProfesional = $resultado[4];
        $this -> foto = $resultado[5];
    }
    
    public function registrar(){
        $this -> conexion -> abrir();
        echo $this -> medicoDAO -> registrar();
        $this -> conexion -> ejecutar($this -> medicoDAO -> registrar());
        $this -> conexion -> cerrar();
    }
    
    public function cambiarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> medicoDAO -> cambiarEstado());
        $this -> conexion -> cerrar();
    }

    
}

?>
