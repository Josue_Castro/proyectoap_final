<?php
require_once "persistencia/conexion.php";
require_once "persistencia/FechasGraficaDAO.php";
class FechasGrafica{
    private $Fecha;
    private $Cantidad;
    private $conexion;
    private $FechasGraficaDAO;
    
    public function getFecha()
    {
        return $this->Fecha;
    }

    public function getCantidad()
    {
        return $this->Cantidad;
    }
    
    
    public function FechasGrafica($Fecha="",$Cantidad=""){
        $this->Fecha=$Fecha;
        $this->Cantidad=$Cantidad;
        $this -> conexion = new conexion();
        $this -> FechasGraficaDAO = new FechasGraficaDAO($this->Fecha,$this->Cantidad);
        
    }
    
    
    public function GraficoFechas(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> FechasGraficaDAO -> GraficoFechas());
        $Datos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new FechasGrafica($resultado[0],$resultado[1]);
            array_push($Datos, $p);
        }
        $this -> conexion -> cerrar();
        return $Datos;
    }


}

?>
