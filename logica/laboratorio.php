<?php
require_once "persistencia/conexion.php";
require_once "persistencia/laboratorioDAO.php";

class laboratorio{

    private $idLaboratorio;
    private $IdCliente;
    private $Fecha;
    private $Colesterol;
    private $ob_Colesterol;
    private $Trigliceridos;
    private $ob_Trigliceridos;
    private $Hemoglobina;
    private $ob_Hemoglobina;
    private $Linfositos;
    private $ob_Linfositos;
    private $Glucosa;
    private $ob_Glucosa;
    private $Observaciones;
    private $IdAnalista;
    private $idCita;
    private $conexion;
    private $laboratorioDAO;



    public function getIdLaboratorio()
    {
        return $this -> idLaboratorio;
    }

    public function getIdCliente()
    {
        return $this -> IdCliente;
    }

    public function getFecha()
    {
        return $this -> Fecha;
    }

    public function getColesterol()
    {
        return $this -> Colesterol;
    }

    public function getTrigliceridos()
    {
        return $this -> Trigliceridos;
    }

    public function getHemoglobina()
    {
        return $this -> Hemoglobina;
    }

    public function getLinfositos()
    {
        return $this -> Linfositos;
    }

    public function getGlucosa()
    {
        return $this -> Glucosa;
    }

    public function getob_Colesterol()
    {
        return $this -> ob_Colesterol;
    }

    public function getob_Trigliceridos()
    {
        return $this -> ob_Trigliceridos;
    }

    public function getob_Hemoglobina()
    {
        return $this -> ob_Hemoglobina;
    }

    public function getob_Linfositos()
    {
        return $this -> ob_Linfositos;
    }

    public function getob_Glucosa()
    {
        return $this -> ob_Glucosa;
    }

    public function getObservaciones()
    {
        return $this -> Observaciones;
    }

    public function getIdAnalista()
    {
        return $this -> IdAnalista;
    }

    public function getIdCita()
    {
        return $this->idCita;
    }




    public function laboratorio($idLaboratorio="",$Fecha="",$Colesterol="", $ob_Colesterol="",$Trigliceridos="", $ob_Trigliceridos="",$Hemoglobina="", $ob_Hemoglobina="",$Linfositos="", $ob_Linfositos="",$Glucosa="", $ob_Glucosa="",$Observaciones="", $IdCliente="",$IdAnalista="",$idCita=""){
        $this -> idLaboratorio = $idLaboratorio;
        $this -> Fecha = $Fecha;
        $this -> Colesterol = $Colesterol;
        $this -> ob_Colesterol = $ob_Colesterol;
        $this -> Trigliceridos = $Trigliceridos;
        $this -> ob_Trigliceridos = $ob_Trigliceridos;
        $this -> Hemoglobina = $Hemoglobina;
        $this -> ob_Hemoglobina = $ob_Hemoglobina;
        $this -> Linfositos = $Linfositos;
        $this -> ob_Linfositos = $ob_Linfositos;
        $this -> Glucosa = $Glucosa;
        $this -> ob_Glucosa = $ob_Glucosa;
        $this -> Observaciones = $Observaciones;
        $this -> IdCliente = $IdCliente;
        $this -> IdAnalista = $IdAnalista;
        $this->idCita=$idCita;
        $this -> conexion = new conexion();
        $this -> laboratorioDAO = new laboratorioDAO($this -> idLaboratorio, $this -> Fecha, $this -> Colesterol, $this -> ob_Colesterol, $this -> Trigliceridos, $this -> ob_Trigliceridos, $this -> Hemoglobina, $this -> ob_Hemoglobina, $this -> Linfositos, $this -> ob_Linfositos, $this -> Glucosa, $this -> ob_Glucosa, $this -> Observaciones
        , $this -> IdCliente,$this -> IdAnalista,$this->idCita);
    }

    public function registrar(){
        $this -> conexion -> abrir();
        //echo $this -> laboratorioDAO -> registrar();
        $this -> conexion -> ejecutar($this -> laboratorioDAO -> registrar());
        $this -> conexion -> cerrar();
    }

    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> laboratorioDAO -> consultarTodos());
        $lab = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new laboratorio($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], $resultado[6], $resultado[7], $resultado[8], $resultado[9], $resultado[10], $resultado[11], $resultado[12]
            , $resultado[13]);
            array_push($lab, $p);
        }
        $this -> conexion -> cerrar();
        return $lab;
    }

    public function consultarLaboratoriopdf($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> laboratorioDAO -> consultarLaboratoriopdf($id));
        $lab = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new laboratorio($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], $resultado[6], $resultado[7], $resultado[8], $resultado[9], $resultado[10], $resultado[11], $resultado[12]
            , $resultado[13], $resultado[14]);
            array_push($lab, $p);
        }
        $this -> conexion -> cerrar();
        return $lab;
    }



    public function consultarHistoria(){
        $this -> conexion -> abrir();
       // echo $this -> laboratorioDAO -> consultarHistoria();
        $this -> conexion -> ejecutar($this -> laboratorioDAO -> consultarHistoria());
        $lab = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new laboratorio($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], $resultado[6], $resultado[7], $resultado[8], $resultado[9], $resultado[10], $resultado[11], $resultado[12]
            , $resultado[13], $resultado[14]);
            array_push($lab, $p);
        }
        $this -> conexion -> cerrar();
        return $lab;
    }




    public function consultarLaboratorio(){
        $this -> conexion -> abrir();
        //echo $this -> laboratorioDAO -> consultarLaboratorio();
        $this -> conexion -> ejecutar($this -> laboratorioDAO -> consultarLaboratorio());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> Fecha = $resultado[0];
        $this -> Colesterol = $resultado[1];
        $this -> ob_Colesterol = $resultado[2];
        $this -> Trigliceridos = $resultado[3];
        $this -> ob_Trigliceridos = $resultado[4];
        $this -> Hemoglobina = $resultado[5];
        $this -> ob_Hemoglobina = $resultado[6];
        $this -> Linfositos = $resultado[7];
        $this -> ob_Linfositos = $resultado[8];
        $this -> Glucosa = $resultado[9];
        $this -> ob_Glucosa = $resultado[10];
        $this -> Observaciones = $resultado[11];
        $this -> IdAnalista = $resultado[12];


    }


    public function consultarGrafico(){
        $this -> conexion -> abrir();
        //echo $this -> laboratorioDAO -> consultarGrafico();
        $this -> conexion -> ejecutar($this -> laboratorioDAO -> consultarGrafico());
        $lab = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new laboratorio($resultado[0],$resultado[1], $resultado[2],"", $resultado[3],"", $resultado[4], "", $resultado[5], "", $resultado[6], "",""
                ,$resultado[7]);
            array_push($lab, $p);
        }
        $this -> conexion -> cerrar();
        return $lab;
    }

}


?>
