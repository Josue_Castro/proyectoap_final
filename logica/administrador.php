    <?php
require_once "persistencia/conexion.php";
require_once "persistencia/administradorDAO.php";

class administrador{
    private $idAdministrador;
    private $nombre;
    private $apellido;
    private $estado;
    private $correo;
    private $clave;
    private $foto;
    private $conexion;
    private $administradorDAO;

    public function getIdAdministrador(){
        return $this -> idAdministrador;
    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function getApellido(){
        return $this -> apellido;
    }

    public function getEstado(){
        return $this -> estado;
    }

    public function getCorreo(){
        return $this -> correo;
    }

    public function getClave(){
        return $this -> clave;
    }

    public function getFoto(){
        return $this -> foto;
    }

    public function administrador($idAdministrador = "", $nombre = "", $apellido = "", $estado="",$correo = "", $clave = "", $foto = ""){
        $this -> idAdministrador = $idAdministrador;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> estado = $estado;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> foto = $foto;
        $this -> conexion = new conexion();
        $this -> administradorDAO = new administradorDAO($this -> idAdministrador, $this -> nombre, $this -> apellido, $this -> estado,$this -> correo, $this -> clave, $this -> foto);
    }

    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> autenticar());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> idAdministrador = $resultado[0];
            $this -> estado = $resultado[1];
            return true;
        }else {
            return false;
        }
    }

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];
        $this -> foto = $resultado[3];
    }

    public function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> registrar());
        $this -> conexion -> cerrar();
    }

    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> consultarTodos());
        $admins = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new administrador($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4]);
            array_push($admins, $p);
        }
        $this -> conexion -> cerrar();
        return $admins;
    }

    public function cambiarEstado(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> administradorDAO -> cambiarEstado());
        $this -> conexion -> cerrar();
    }

}

?>
